<?php session_start(); ?>
<!-- Head/Navbar -->
<?php require_once('inc/head.php'); ?>

<!-- Header -->
<header class="header">
	<div id="particles-js" class="particules"></div>
	<div class="container">
		<div class="content-header" data-aos="fade-down">
			<img src="assets/img/logo/logo.png" class="img-responsive header-logo" alt="logo">
			<!-- <p href="#" class="connect-serveur">Rejoignez nos <span id="players_count">0</span> joueurs</p> -->
			<p href="#" class="connect-serveur" style="color: #fff;"><!-- <i class="fas fa-cog fa-spin" style="color: #ff7a7a;"></i> --><i class="fas fa-info" style="color: #fff;    letter-spacing: 8px;"></i> Actuellement ouvert</p>
<!-- 			<a class="connect-serveur disabled" style="background: #ffa50054;color: white;padding: 24px;">Bêta fermée complete !</a> -->
		</div>
	</div>
</header>

<!-- Content Accueil -->
<section class="section pattern-p1">
	<div class="container">

		<div class="content-actus">

			<div class="row">
				<div class="col-lg-9" data-aos="fade-right" data-aos-duration="2000">

				<div class="youtube-home">
					<h4 class="title" data-aos="fade-down" data-aos-duration="2000" style="margin: 0% 0% 3% 0%;text-align: center;">Bienvenue sur Vultanium !</h1>
					<div class="row">

						<div class="col-md-8">
							<div class="video-container">
		            			<iframe width="560" height="315" src="https://www.youtube.com/embed/fr4iDsZ66Gg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin: auto;display: block;"></iframe>
		            		</div>
		            	</div>

						<div class="col-md-4">

						<?php if(isset($_SESSION['auth'])): ?>
							<h4 class="title-home-first">Espace membre :</h4>
							<a href="https://vultanium.fr/membres/compte" class="btn btn-primary btn-home-first btn-read-more" style="background: #328417;"><i class="fas fa-user" style="color:#fff"></i> Bienvenue <?= $_SESSION['auth']->username; ?></a>
							<a href="https://vultanium.fr/membres/deconnexion" class="btn btn-primary btn-home-first btn-read-more" style="background: #3463a9;"><i class="fas fa-sign-out-alt" style="color:#fff;"></i> Déconnexion</a>
						<?php else: ?>
							<h4 class="title-home-first">Espace membre :</h4>
							<a href="https://vultanium.fr/membres/inscription" class="btn btn-primary btn-home-first btn-read-more" style="background: #328417;"><i class="fas fa-user-plus" style="color:#fff"></i> Inscription</a>
							<a href="https://vultanium.fr/membres/connexion" class="btn btn-primary btn-home-first btn-read-more" style="background: #3463a9;"><i class="fas fa-user-lock" style="color:#fff"></i> Me connecter</a>
						<?php endif; ?>


							<h4 class="title-home-first" style="margin-top: 10%;">Utilitaires :</h4>
							<a href="https://vultanium.buycraft.net/" target="_blank" class="btn btn-primary btn-home-first btn-read-more"><i class="fas fa-shopping-cart" style="color:#fff"></i> Notre Boutique</a>
							<a href="https://vultanium.fr/pages/jouer" class="btn btn-primary btn-home-first btn-read-more"><i class="fas fa-download" style="color:#fff"></i> Notre RessourcePack</a>
							<a href="https://vultanium.fr/pages/jouer" class="btn btn-primary btn-home-first btn-read-more"><i class="fas fa-tablet-alt" style="color:#fff"></i> Notre Launcher</a>

		            	</div>

					</div>		
				</div>

				<h4 class="title" data-aos="fade-down" data-aos-duration="2000" style="margin: 0% 0% 3% 0%;">Nos dernières actualités</h1>
					<div class="row">

						<div class="card col-lg-4">
							<a href="pages/actus/beta-fermee.php"><img class="card-img-top" src="assets/img/actu-beta.png" alt="V2"></a>
							<div class="card-body">
								<h2 class="card-title">Bêta fermée !</h2>
								<p class="card-text">La bêta fermée est bientôt prête à vous accueilir, plus que quelques jours... Avez vous hâte de la découvrir ?!</p>
								<a href="pages/actus/beta-fermee.php" class="btn btn-primary btn-read-more">En savoir plus »</a>
							</div>
							<div class="card-footer text-muted">
								Posté le 03/01/19, par
								<a href="#">Vultanium</a>
							</div>
						</div>

						<div class="card col-lg-4">
							<a href="pages/actus/v2-vultanium.php"><img class="card-img-top" src="assets/img/v2-vultanium.png" alt="V2"></a>
							<div class="card-body">
								<h2 class="card-title">Saison 2 en approche !</h2>
								<p class="card-text">La saison 2 de Vultanium (une des plus importante), approche à petits pas... Quelles sont les nouveautés de cette saison ?!</p>
								<a href="pages/actus/v2-vultanium.php" class="btn btn-primary btn-read-more">En savoir plus »</a>
							</div>
							<div class="card-footer text-muted">
								Posté le 09/11/18, par
								<a href="#">Vultanium</a>
							</div>
						</div>

						<div class="card col-lg-4">
							<a href="pages/recrutement.php"><img class="card-img-top" src="assets/img/actus-recrutement.png" alt="V2"></a>
							<div class="card-body">
								<h2 class="card-title">Recrutement - Saison 2</h2>
								<p class="card-text">Les recrutements de la saison 2 sont ouverts ! Nous recherchons modérateurs, architectes, assistants, graphistes et bien d'autres...</p>
								<a href="pages/recrutement.php" class="btn btn-primary btn-read-more">Postuler »</a>
							</div>
							<div class="card-footer text-muted">
								Posté le 15/11/18, par
								<a href="#">Vultanium</a>
							</div>
						</div>

					</div>
				</div>
				<div class="col-lg-3" data-aos="fade-left" data-aos-duration="2000">
					<a class="twitter-timeline" data-height="1000" data-link-color="#ee9831" href="https://twitter.com/vultanium?ref_src=twsrc%5Etfw">Tweets by vultanium</a>
					<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
				</div>
			</div>
		</div>

	</div>
</section>

<!-- Footer -->
<?php include('inc/footer.php'); ?>