        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="row ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <div class="ft-logo"><p class="text-footer">Réseau de serveurs minecraft depuis 2018</p></div>
                    </div>
                </div>
                <hr class="footer-line">
                <div class="row ">
                    <!-- footer-about -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Mentions légales</div>
                            <ul class="list-unstyled">
                                <li><a href="/pages/cgu.php">CGU</a></li>
                                <li><a href="/pages/cgv.php">CGV</a></li>
                                <li><a href="/pages/reglement.php">Règlement</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- /.footer-about -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Vultanium</div>
                            <ul class="list-unstyled">
                                <li><a href="/pages/recrutement.php">Recrutement</a></li>
                                <li><a href="/pages/partenaire.php">Partenaires</a></li>
                                <li><a href="/pages/support.php">Support</a></li>
                                <li><a href="/pages/status.php">Status des services</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- /.footer-links -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                            <div class="footer-title">Nous suivre</div>
                            <ul class="list-unstyled">
                                <li><a href="https://twitter.com/vultanium" target="_blank"><i class="fab fa-twitter" style="margin-right: 5px;"></i> Twitter</a></li>
                                <li><a href="https://www.youtube.com/channel/UCUFmV9ZL_01H5gqN4luYfmQ" target="_blank"><i class="fab fa-youtube" style="margin-right: 5px;"></i> Youtube</a></li>
                                <li><a href="https://www.instagram.com/vultanium/" target="_blank"><i class="fab fa-instagram" style="margin-right: 5px;"></i> Instagram</a></li>
                                <li><a href="https://discord.gg/bUSvAev" target="_blank"><i class="fab fa-discord" style="margin-right: 5px;"></i> Discord</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- /.footer-links -->
                    <!-- footer-links -->
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-6 col-6 ">
                        <div class="footer-widget ">
                        	<img src="/assets/img/logo/logo.png" class="img-responsive" style="width: 44%;">
                        </div>
                    </div>

                    <!-- /.footer-links -->
                    <!-- tiny-footer -->
                </div>
                <div class="row ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center ">
                        <div class="tiny-footer">
                            <p class="copyright-text">Numero de dépôt:<a href="http://vultanium.copyright01.com" target="_blank"> V596E4</a> - Copyright Vultanium - 2018 | Design & Developpement par <a href="https://alex-m.eu" target="_blank" class="copyrightlink">AlexM</a></p>
                            <p class="copyright-text">Vultanium n'est en aucun cas affilié à Mojang & Microsoft.</p>
                        </div>
                    </div>
                    <!-- /. tiny-footer -->
                </div>
            </div>
        </div>
        <!-- /.footer -->
        
        <!-- Scripts et fichiers JS -->
        
        <!-- Bootstrap -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        
        <!-- AOS -->
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>AOS.init();</script>
        
        <!-- Loader -->
        <script src="/assets/js/loader.js"></script>
        
        <!-- Modals -->
        <script type="text/javascript">
            $('#modalVirtuose').modal({
              keyboard: false
            })
        </script>

        <!-- Particules -->
        <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
        <script src="/assets/js/particles.js"></script>
        
        <!-- Text animé -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js" integrity="sha256-BuxrUdr/4YoztQLxT6xmdO6hSQw2d6BtBUY1pteGds4=" crossorigin="anonymous"></script>
        <script src="/assets/js/animated-text.js"></script>
        <script src="/assets/js/anime-text.js"></script>
        <script src="/assets/js/firefly.js"></script>
        
        <!-- Afficher joueurs en ligne -->
<!--         <script src="https://www.serveurs-minecraft.org/api/players_count.php?id=54879&amp;format=jsonp"></script>
        <script src="https://www.serveurs-minecraft.org/api/slots_count.php?id=54879&amp;format=jsonp"></script> -->
        <!-- script src="/assets/js/player_connect.js"></script -->
        
        <!-- Afficher nombres de vote -->
<!--         <script src="https://www.serveurs-minecraft.org/api/votes_count.php?id=54879&amp;period=month&amp;format=jsonp"></script> -->
        
        <!-- API Google -->
        <script src="https://apis.google.com/js/platform.min.js"></script>

        <!-- Cookies -->
        <script src="/assets/js/cookiechoices.js"></script>
        <script>document.addEventListener('DOMContentLoaded', function(event){cookieChoices.showCookieConsentBar('Ce site utilise des cookies pour vous offrir le meilleur service. En poursuivant votre navigation, vous acceptez l’utilisation des cookies.', 'J’accepte', 'En savoir plus', 'https://vultanium.fr/pages/cgu.php');});</script>
        
        <!-- Sentry JS SDK 2.1.+ required -->
        <script src="https://cdn.ravenjs.com/3.27.0/raven.min.js" integrity="sha384-gnq8j+n3z7Vfgsl2ZmIRZtumzS05EKAhNoPa4vDhcq3rg1Pz6Tdg3LyYxcxTzzV8" crossorigin="anonymous"></script>
        <script>
            // configure the SDK as you normally would
            Raven.config('https://75aa67a314d54d489cab7481065a57c1@sentry.io/1322431', { release: '0e4fdef81448dcfa0e16ecc4433ff3997aa53572' }).install();
            function handleRouteError(err) {
              Raven.captureException(err);
              Raven.showReportDialog();
            };
            try {
                //renderRoute()
            } catch (err) {
                handleRouteError(err);
            }
        </script>
    </body>
</html>