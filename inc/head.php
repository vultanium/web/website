<?php
session_start();

require_once 'sentry/Raven/Autoloader.php';
Raven_Autoloader::register();

$client = new Raven_Client('https://75aa67a314d54d489cab7481065a57c1@sentry.io/1322431');
$error_handler = new Raven_ErrorHandler($client);
$error_handler->registerExceptionHandler();
$error_handler->registerErrorHandler();
$error_handler->registerShutdownFunction();
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name=viewport content="width=device-width, initial-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
    
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-transcluent" />
    
        <!-- Favicon -->
        <link rel="icon" href="/favicon.ico" />
    
        <!-- Title -->
        <title>Vultanium - SemiRP Customisé | Minecraft</title>
    
        <!-- JQuery -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-tsQFqpEReu7ZLhBV2VZlAu7zcOV+rXbYlF2cqB8txI/8aZajjp4Bqd+V6D5IgvKT" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" integrity="sha256-0rguYS0qgS6L4qVzANq4kjxPLtvnp5nn2nB5G1lWRv4=" crossorigin="anonymous"></script>

        <script src="https://www.google.com/recaptcha/api.js"></script>

        <!-- IE-Support -->
        <script src="/assets/js/ie-support.js" defer></script>
    
        <!-- Player Count -->
        <script src="https://cdn.rawgit.com/leonardosnt/mc-player-counter/1.1.0/dist/mc-player-counter.min.js"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135434559-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-135434559-1');
        </script>

        <!-- CSS-files -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo|Poppins">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css">
        <link rel="stylesheet" href="/assets/css/theme.css">
    </head>
    <body>
        <!-- Loader -->
        <div class="loader"></div>

        <!-- Menu navbar -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar" style="margin: 0% 3%;">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item">
                <a class="nav-link" href="/accueil.php"><i class="fas fa-home"></i> Accueil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pages/jouer.php"><i class="fab fa-fort-awesome-alt"></i> Jouer</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="https://vultanium.buycraft.net" target="_blank"><i class="fas fa-shopping-cart"></i> Boutique</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pages/vote.php"><i class="fas fa-check-square"></i> Voter</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pages/support.php"><i class="fas fa-question-circle"></i> Support</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pages/changelogs.php"><i class="fas fa-code-branch"></i> Versions</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><i class="fas fa-shopping-bag"></i> Magasin <span class="badge badge-secondary" style="background: #ee9831;">Soon</span></a>
              </li>
            </ul>

            <!-- Espace membres / Connexion - Inscription -->
<?php if(isset($_SESSION['auth'])): ?>
            <a href="/membres/compte.php" class="btn btn-success btn-membre"><img src="https://mc-heads.net/avatar/<?= $_SESSION['auth']->username; ?>/30" style="margin-right: 10px;">Bonjour <?= $_SESSION['auth']->username; ?></a>
            <a href="/membres/deconnexion.php" class="btn btn-success btn-logout"><i class="fa fa-sign-out-alt"></i></a>
<?php else: ?>
            <a href="/membres/connexion.php" class="btn btn-success btn-nav-member btn-sign"><i class="fas fa-user-lock" style="color: #fff;"></i></a>
<?php endif; ?>
          </div>
        </nav>