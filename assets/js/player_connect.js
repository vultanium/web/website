window.onload = function () {
    var players = document.getElementById("players_count") || null;
    var slots = document.getElementById("slots_count") || null;

    if (players !== null || slots !== null) {
        if (serveurs_minecraft_org_players_count >= 0 && serveurs_minecraft_org_slots_count >= 0) {
            players.innerHTML = serveurs_minecraft_org_players_count;
            slots.innerHTML = serveurs_minecraft_org_slots_count;
        } else {
            players.innerHTML = '?';
            slots.innerHTML = '?';
        }
    }
}