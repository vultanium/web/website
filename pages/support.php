<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
    <div class="container">
        <div class="pages-header">
            <h1 class="ml6">
              <span class="text-wrapper">
                <span class="letters">Support général</span>
              </span>
            </h1>
        </div>
    </div>
</header>

<!-- Content Support -->
<section class="section pattern-p1">
    <div class="container">
    <h4 class="title" data-aos="fade-down" data-aos-duration="3000">Besoin d'aides ?</h4>
        <div class="row">
            <div class="col-md-8" data-aos="fade-right" data-aos-duration="3000">

                <div class="accordion" id="faqExample" style="margin-top: 25px;">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link q-faq" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1. Je n'arrive pas à me connecter au serveur
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faqExample">
                            <div class="card-body">
                                Vérifier que vous êtes bien connecté avec la bonne version (1.10 à 1.12.2), vérifier que vous n'ayez pas fais de faute dans l'adresse IP : <b>play.vultanium.fr</b>, vous pouvez aussi utiliser notre launcher disponible sur le site, 
                                si le problème persiste contacter nous sur notre <a href="https://discord.vultanium.fr" target="_blank">discord</a> en précisant votre problème dans un channel de support.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                            <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            2. Je rencontre des soucis de permissions ou d'utilisation
                            </button>
                        </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                            <div class="card-body">
                                Si vous rencontrez un de ces soucis, signalez le dans les channels de support sur notre <a href="https://discord.vultanium.fr" target="_blank">discord</a> en indiquant
                                la commande qui ne fonctionne pas et sur quel serveur.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                            <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                            3. J'ai trouvé un bug ou une faille
                            </button>
                        </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">
                            <div class="card-body">
                                Si vous rencontrez un bug ou si vous découvrez une faille, signalez le dans les channels de support sur notre <a href="https://discord.vultanium.fr" target="_blank">discord</a> en donnant
                                le plus d'informations possible. <b>Attention</b>, n'exploiter pas le bug sous peine de sanction.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                4. Je n'ai pas encore reçu mon bien immatériel acheté sur la boutique
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                            <div class="card-body">
                                Les biens immatériels peuvent mettre jusqu'à 15 minutes maximum avant d'être délivré sur le serveur. Si au delà de ce délai vous n'avez toujours pas reçu votre bien, contacter
                                nous sur <a href="https://discord.vultanium.fr" target="_blank">discord</a> dans le channel <b>#support-boutique</b> en indiquant votre pseudo et l'achat effectué.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                5. Je souhaite être remboursé d'un bien acheté sur la boutique
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                            <div class="card-body">
                                Comme l'indique nos <a href="cgv.php" target="_blank">CGV</a>, article n°12, aucun remboursement n'est possible après chaque achat.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                                6. Je souhaiterai être unban du serveur
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                            <div class="card-body">
                                Pour négocier un unban sur notre serveur, contacter un membre du staff sur <a href="https://discord.vultanium.fr" target="_blank">discord</a>, selon la gravité et la raison
                                de votre ban, une réduction de sanction pourra être mise en place.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link q-faq collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseThree">
                                7. Je n'ai pas le RessourcePack du serveur en jeu
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">
                            <div class="card-body">
                                Si vous n'arrivez pas à avoir le RessourcePack en vous connectant au serveur, vérifier que vous l'avez bien activé en entrant l'IP du serveur. Sinon vous
                                pouvez le télécharger sur notre site : <a href="https://vultanium.fr/pages/jouer">vultanium.fr/jouer</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-4" data-aos="fade-left" data-aos-duration="3000">
                <iframe src="https://discordapp.com/widget?id=445251773288939540&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" class="discord"></iframe>
                <a href="https://discord.gg/bUSvAev" target="_blank" class="btn btn-success btn-discord"><i class="fas fa-arrow-circle-right" style="color:white;margin-right: 10px;"></i>Rejoindre le discord</a>
            </div>

        </div>
    </div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
