<?php session_start(); ?>
<!-- Header / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
    <div class="container">
        <div class="pages-header">
            <h1 class="ml6">
              <span class="text-wrapper">
                <span class="letters">Changelogs</span>
              </span>
            </h1>
        </div>
    </div>
</header>

<!-- Changelogs -->

<!-- 1.4 -->
<section class="section pattern-p1">
    <div class="container">
        
        <table class="table cg-table">
          <thead class="cg-thead">
            <tr>
              <th scope="col">Version</th>
              <th scope="col">Date</th>
              <th scope="col">Type</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">0.1.0</th>
              <td>26/01/19</td>
              <td>Nouveauté</td>
              <td>Mise en place de la bêta fermée</td>
            </tr>
          </tbody>
        </table>

    </div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>