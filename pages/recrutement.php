<?php session_start(); ?>
<?php
require '../modules/captcha.php';
$captcha = new Recaptcha('6LfgF48UAAAAAOBIruNyMeKB_6y28FzwNrXEQvfQ', '6LfgF48UAAAAAM30bljr0A599ZK6PTHrowiwi1Lr');
?>


<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Recrutement</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Postulez -->
<section class="section pattern-p1">
    <div class="container">
        <h4 class="title" data-aos="fade-down" data-aos-duration="2000">Postuler chez Vultanium</h4>
        <p data-aos="fade-down" data-aos-duration="2000">Nous recherchons des personnes mature, active, motivé et intéressé par notre serveur. Il faut être âgé de 16 ans minimum, réponse par mail sous un délai de 72 heures maximum.</p>
        <p style="font-size: 14px;" data-aos="fade-down" data-aos-duration="2000">Vous êtes intéressez par le serveur Vultanium ? Vous souhaitez postulez pour rejoindre notre équipe ? Remplissez ce formulaire avec de vraies informations, une réponse ainsi qu'un entretien vocal vous sera donné si votre candidature est retenu.</p>

        <hr>

<!-- ======================================== -->

<?php

$prenom=$_POST['prenom'];
$pseudo=$_POST['pseudo'];
$email=$_POST['email'];
$age=$_POST['age'];
$centres=$_POST['centres'];
$horaires=$_POST['horaires'];
$discord=$_POST['discord'];
$premium=$_POST['premium'];
$minecraft=$_POST['minecraft'];
$decouverte=$_POST['decouverte'];
$attirance=$_POST['attirance'];
$poste=$_POST['poste'];
$mature=$_POST['mature'];
$motivation=$_POST['motivation'];

 
$headers = "Vous avez reçu une candidature de $prenom pour le poste : $poste";

$destinataire="vultanium@gmail.com, retro@vultanium.fr, leo@vultanium.fr, neoztoxic@vultanium.fr";

$body="

PRENOM : $prenom \r\n
PSEUDO MC : $pseudo \r\n
EMAIL : $email \r\n
AGE : $age \r\n

» Postule pour le poste de $poste \r\n

• Centre d'intérêts : \r\n
$centres

\r\n
• Horaires de connexion : \r\n
$horaires

\r\n
PSEUDO DISCORD : $discord \r\n
MINECRAFT PREMIUM : $premium \r\n

• Pourquoi je joue à Minecraft : \r\n
$minecraft
\r\n

• Comment j'ai découvert le serveur : \r\n
$decouverte
\r\n

• Qu'est-ce qui m'attire sur Vultanium : \r\n
$attirance
\r\n

• Suis-je assez mature pour ce poste : \r\n
$mature
\r\n

• Mes motivations : \r\n
$motivation

";


if (!empty($_POST)) {
    if($captcha->isValid($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']) === false){
        ?>
        <div class="alert alert-danger" style="width: 60%;margin: auto;display: block;padding: 1%;margin-bottom: 2%;">
            Le CAPTCHA est invalide. Veuillez le cocher.
        </div>
        <?php

    } else {

		if (mail($destinataire,$headers,$body)) {	
			header('Location:recrutement.php');
		}

        ?>
        <div class="alert alert-success" style="width: 60%;margin: auto;display: block;padding: 1%;margin-bottom: 2%;">
            Votre candidature nous a bien été envoyé. Nous vous remercions.
        </div>
    <?php
    }
}
?>

<!-- ============================================================== -->

		<form action="" method="POST" data-aos="fade-up" data-aos-duration="2000">

		  <div class="form-row">

		    <div class="form-group col-md-6">
		      <label for="prenom">Votre prénom <small style="color:red;">*</small></label>
		      <input type="text" class="form-control" id="prenom" name="prenom" required>
		    </div>


		<?php if(isset($_SESSION['auth'])): ?>
		    <div class="form-group col-md-6">
		      <label for="pseudo">Votre pseudo Minecraft <small style="color:red;">*</small></label>
		      <input type="text" class="form-control" id="pseudo" name="pseudo" value="<?= $_SESSION['auth']->username; ?>" required>
		    </div>
		<?php else: ?>
		    <div class="form-group col-md-6">
		      <label for="pseudo">Votre pseudo Minecraft <small style="color:red;">*</small></label>
		      <input type="text" class="form-control" id="pseudo" name="pseudo" required>
		    </div>
		<?php endif; ?>


		  </div>

		  <div class="form-row">


		<?php if(isset($_SESSION['auth'])): ?>
			<div class="form-group col-md-6">
			  <label for="email">Votre adresse mail <small style="color:red;">*</small></label>
			  <input type="email" class="form-control" id="email" name="email" value="<?= $_SESSION['auth']->email; ?>" required>
			</div>
		<?php else: ?>
			<div class="form-group col-md-6">
			  <label for="email">Votre adresse mail <small style="color:red;">*</small></label>
			  <input type="email" class="form-control" id="email" name="email" required>
			</div>
		<?php endif; ?>


			  <div class="form-group col-md-6">
			    <label for="age">Votre âge <small style="color:red;">*</small></label>
			    <input type="number" class="form-control" id="age" name="age" required>
			  </div>

		  </div>

		  <hr>

		  <div class="form-group">
		    <label for="centres">Vos centres d'intérêts</label>
		    <textarea class="form-control" id="centres" name="centres" rows="3"></textarea>
		  </div>

		  <div class="form-group">
		    <label for="horaires">Vos horaires de connexion <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="horaires" name="horaires" rows="2" required></textarea>
		  </div>

		  <div class="form-group">
		    <label for="social">Quels réseaux sociaux utilisez-vous ? <small>(Ctrl + clic pour en séléctionner plusieurs)</small></label>
		    <select multiple class="form-control" name="social" id="social">
		      <option>Facebook</option>
		      <option>Twitter</option>
		      <option>Youtube</option>
		      <option>Discord</option>
		      <option>Skype</option>
		    </select>
		  </div>

		  <hr>

		  <div class="form-row">

			  <div class="form-group col-md-6">
			    <label for="discord">Votre pseudo Discord avec le # <small style="color:red;">*</small></label>
			    <input type="text" class="form-control" id="discord" name="discord" required>
			  </div>		  
	       	
			  <div class="form-group col-md-6">
			    <label for="premium">Avez-vous Minecraft Premium ? <small style="color:red;">*</small></label>
			    <select class="form-control" id="premium" name="premium" required>
			      <option>Oui</option>
			      <option>Non</option>
			    </select>
			  </div>

		  </div>

		  <hr>

		  <div class="form-group">
		    <label for="minecraft">Pourquoi jouez-vous à Minecraft ? <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="minecraft" name="minecraft" rows="3" required></textarea>
		  </div>

		  <div class="form-group">
		    <label for="decouverte">Comment avez-vous découvert Vultanium ? <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="decouverte" name="decouverte" rows="2" required></textarea>
		  </div>

		  <div class="form-group">
		    <label for="attirance">Qu'est-ce qui vous attire sur Vultanium ? <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="attirance" name="attirance" rows="3" required></textarea>
		  </div>

		  <hr>

		  <div class="form-group">
		    <label for="poste">Pour quel poste souhaitez-vous postuler ? <small style="color:red;">*</small></label>
		    <select class="form-control" id="poste" name="poste" required>
		      <option>Assistant</option>
		      <option>Architecte</option>
		      <option>Modérateur</option>
		      <option>Graphiste</option>
		      <option>Chargé de communication</option>
		      <option>Développeur</option>
		    </select>
		  </div>

		  <div class="form-group">
		    <label for="mature">Pensez-vous être assez mature, responsable pour ce poste ? <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="mature" name="mature" rows="2" required></textarea>
		  </div>

		  <div class="form-group">
		    <label for="motivation">Quels sont vos motivations ? <small style="color:red;">*</small></label>
		    <textarea class="form-control" id="motivation" name="motivation" rows="3" required></textarea>
		  </div>

		  <hr>

		  <?= $captcha->html(); ?>
		  <input type="submit" name="send-recrutement" id="send-recrutement" class="btn btn-recrutement" value="Envoyer ma candidature">

		</form>


    </div>
</section>


<!-- ------- -->

<!-- Footer -->
<?php include('../inc/footer.php'); ?>

<!--         <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScRluVkyH7kWuItrkHpGtUUQ4ZR7SrGo5Bqq8yhm69lO-6etw/viewform" style="width: 100%;height: 1800px;"></iframe> -->

<!-- , retro@vultanium.fr, neoztoxic@vultanium.fr, leoleclercleo@gmail.com -->