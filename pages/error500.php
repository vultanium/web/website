<?php include('../inc/head.php'); ?>

<!-- 500 -->
<header class="header-404 color_overlay d-flex align-items-center parallax-error">
    <div class="container">
        <div class="wrapper-404-alert"
             data-aos="flip-right"
             data-aos-delay="700"
             data-aos-easing="ease-in"
             data-aos-duration="600">
            <div class="content-error">
                <h2 class="display-2 title-error">Erreur 500</h2>
                <h2>Erreur Interne</h2>
                <p>Ooops une erreur vien d'apparaître.</p>
                <div class="d-flex justify-content-center">
                    <a href="../accueil.php" class="button-back"><i class="fa fa-angle-left"></i>Retour à l'accueil</a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>