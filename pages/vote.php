<?php session_start(); ?>
<!-- Head / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
    <div class="container">
        <div class="pages-header">
            <h1 class="ml6">
              <span class="text-wrapper">
                <span class="letters">Voter pour Vultanium</span>
              </span>
            </h1>
        </div>
    </div>
</header>

<!-- Content Vote -->
<section class="section pattern-p1">
    <div class="container">
        <h4 class="title" data-aos="fade-down" data-aos-duration="2000">Voter pour notre serveur</h4>
        <div class="row" style="padding: 3%;" data-aos="fade-up" data-aos-duration="2000">

            <div class="col-md-4">
                <img src="../assets/img/vote1.png" class="img-responsive" alt="">
                <a href="https://www.serveurs-minecraft.org/vote.php?id=54879" target="_blank" class="btn bt-success btn-vote">serveurs-minecraft.org</a>
            </div>

            <div class="col-md-4">
                <img src="../assets/img/vote2.png" class="img-responsive" alt="">
                <a href="https://www.serveurs-minecraft.com/serveur-minecraft?Classement=Vultanium" target="_blank" class="btn bt-success btn-vote">serveurs-minecraft.com</a>
            </div>

            <div class="col-md-4">
                <img src="../assets/img/vote3.png" class="img-responsive" alt="">
                <a href="https://minecraft.top-serveurs.net/vultanium" target="_blank" class="btn bt-success btn-vote">top-serveurs.net</a>
            </div>

        </div>
    </div>
</section>


<!-- Footer -->
<?php include('../inc/footer.php'); ?>
