<?php session_start(); ?>
<!-- Header / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">play.vultanium.fr</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Content Jouer -->
<!-- ===========================================
				Connecté
=========================================== -->
<?php if(isset($_SESSION['auth'])): ?>

<section class="section pattern-p1">
	<div class="container">
		<h4 class="title" style="text-align: center;" data-aos="fade-down" data-aos-duration="2000">Nous rejoindre sur Vultanium</h4>
		<p class="text-launcher" data-aos="fade-down" data-aos-duration="2000">Télécharger le launcher pour jouer à Vultanium dans les meilleures conditions !</p>

		<div class="content-play" data-aos="fade-up" data-aos-duration="2000">
			<div class="row">

				<div class="col-md-4">
					<p class="dl-text"><i class="fab fa-windows"></i> Télécharger la version Windows</p>
					<a href="https://cdn.vultanium.fr/launcher/builds/release/win/vultanium-launcher-0.0.1.exe" class="btn-launcher"><i class="fab fa-windows" style="color:white;"></i> Télécharger (.exe)</a><br /><br /><br />
<!-- 					<a class="btn-launcher disabled"><i class="fab fa-windows" style="color:white;"></i> Télécharger (.exe) Web installer</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-windows" style="color:white;"></i> Télécharger (.appx)</a><br /><br /><br /> -->
					<a class="btn-launcher disabled"><i class="fab fa-windows" style="color:white;"></i> Télécharger (.exe) Portable</a><br /><br /><br />
<!-- 					<a class="btn-launcher disabled"><i class="fab fa-windows" style="color:white;"></i> Télécharger (.msi)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-windows" style="color:white;"></i> Télécharger (Squirrel)</a> -->
				</div>

				<div class="col-md-4">
					<p class="dl-text"><i class="fab fa-apple"></i> Télécharger la version Mac</p>
					<a class="btn-launcher disabled"><i class="fab fa-apple" style="color:white;"></i> Télécharger (.dmg)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-apple" style="color:white;"></i> Télécharger (.mas)</a><br /><br /><br />
<!-- 					<a class="btn-launcher disabled"><i class="fab fa-apple" style="color:white;"></i> Télécharger (.pkg)</a> -->
				</div>

				<div class="col-md-4">
					<p class="dl-text"><i class="fab fa-linux"></i> Télécharger la version Linux</p>
					<a href="https://cdn.vultanium.fr/launcher/builds/release/linux/vultanium-launcher-0.0.1.AppImage" class="btn-launcher"><i class="fab fa-linux" style="color:white;"></i> Télécharger (.AppImage)</a><br /><br /><br />
<!-- 					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (.snap)</a><br /><br /><br /> -->
					<a href="https://cdn.vultanium.fr/launcher/builds/release/linux/vultanium-launcher-0.0.1.deb" class="btn-launcher"><i class="fab fa-linux" style="color:white;"></i> Télécharger (.deb)</a><br /><br /><br />
<!-- 					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (.rpm)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (FreeBSD)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (PacMan)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (P5P)</a><br /><br /><br />
					<a class="btn-launcher disabled"><i class="fab fa-linux" style="color:white;"></i> Télécharger (.apk)</a> -->
				</div>

			</div>
		</div>
	</div>
</section>

<?php else: ?>
<!-- ===========================================
				Non connecté
=========================================== -->

<!-- Content Jouer -->
<section class="section pattern-p1">
	<div class="container">
		<h4 class="title" style="text-align: center;" data-aos="fade-down" data-aos-duration="2000">Nous rejoindre sur Vultanium</h4>
		<p class="text-launcher" data-aos="fade-down" data-aos-duration="2000">Télécharger le launcher pour jouer à Vultanium dans les meilleures conditions !</p>

		<div class="content-play" data-aos="fade-up" data-aos-duration="2000">
			<p class="dl-text-pack"><i class="fas fa-download"></i> Vous souhaitez télécharger notre launcher ?</p>
			<a class="btn-launcher-pack disabled"><i class="fas fa-download" style="color:white;"></i> Vous devez être connecté pour le télécharger</a>
		</div>
	</div>
</section>

<?php endif; ?>
<!-- =========================================== -->

<hr>

<section class="section pattern-p1">
	<div class="container">
		<h4 class="title" style="text-align: center;" data-aos="fade-down" data-aos-duration="2000">Vous pouvez aussi nous rejoindre via notre adresse IP :</h4>
		<div class="content-play">
			<img src="../assets/img/banner/bannerjoin.png" class="img-responsive img-join" data-aos="fade-up" data-aos-duration="2000">
		</div>
	</div>
</section>

<hr>

<section class="section pattern-p1" id="#pack">
	<div class="container">
		<p class="text-launcher" data-aos="fade-down" data-aos-duration="2000">Télécharger le RessourcePack du serveur si celui-ci ne se met pas automatiquement en vous connectant au serveur</p>

		<div class="content-play" data-aos="fade-up" data-aos-duration="2000">
			<div class="row">

<?php if(isset($_SESSION['auth'])): ?>
				<div class="col-lg-12">
					<p class="dl-text-pack"><i class="fas fa-download"></i> Cliquer sur le bouton ci-dessous pour lancer le téléchargement <small>(Dernière MAJ : 06/02/19)</small></p>
					<a class="btn-launcher-pack" href="https://www.dropbox.com/s/yeltbs2a9plo5vg/VultaPack%20-%200.3.0.zip?dl=1"><i class="fas fa-download" style="color:white;"></i> Télécharger (.zip)</a>
				</div>
<?php else: ?>
				<div class="col-lg-12">
					<p class="dl-text-pack"><i class="fas fa-download"></i> Cliquer sur le bouton ci-dessous pour lancer le téléchargement <small>(Dernière MAJ : 06/02/19)</small></p>
					<a class="btn-launcher-pack disabled"><i class="fas fa-download" style="color:white;"></i> Vous devez être connecté pour le télécharger</a>
				</div>
<?php endif; ?>

			</div>
		</div>

	</div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>