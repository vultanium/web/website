<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">CGV</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Conditions générales de vente -->
<section class="section pattern-p1">
	<div class="container">
	<h4 class="title">Conditions générales de ventes</h4>

		<div class="post-content" style="margin-top: 50px;">
			<h3 class="post-title reglement-title">01. Définitions</h3>
			<p class="text-part"><strong>Services :</strong> désigne l'ensemble des services offerts par Vultanium par l'intermédiaire du site <a href="http://vultanium.fr">http://vultanium.fr</a>, des serveurs de jeux Minecraft et de son serveur de communication vocal Discord. (<a href="https://discord.gg/bUSvAev" target="_blank">http://discord.vultanium.fr</a>)</p>
			<p><strong>Utilisateur :</strong> désigne la personne physique, utilisant les Services offerts par Vultanium.</p>
			<p><strong>Données Personnelles :</strong> désigne les informations personnelles que l'Utilisateur a enregistrées lors de son inscription aux Services proposés par Vultanium et/ou fournies dans le cadre de l'utilisation des Services.</p>
			<p><strong>Droits de Propriété Intellectuelle :</strong> désignent les marques, les noms de domaine, les droits d'auteur, les copyrights, les dessins, les brevets, les droits sur les Bases de Données ou tous autres droits de propriété intellectuelle exploités par Vultanium et nécessaires à ses activités de prestataire de Services.</p>
			<p><strong>Formulaire :</strong> désigne le formulaire d'inscription ou de demande d'informations que l'Utilisateur peut compléter pour pouvoir bénéficier de certains services de Vultanium après avoir pris connaissance des présentes Conditions Générales d'Utilisation.</p>
			<p><strong>Lien Hypertexte :</strong> désigne le système de référencement matérialisé par un mot, une icône ou un logo qui permet par un click de souris de passer d'un document à un autre sur un même site web ou d'une page d'un site web à la page d'un autre site web.</p>
			<p><strong>Site Web :</strong> désigne le site Internet mis à la disposition du public et des Utilisateurs.</p>
			<p><strong>Compte :</strong> désigne les données qui représentent l'Utilisateur sur les Services. Tels que le pseudonyme, l'email, le mot de passe, ...</p>
			<p><strong>Contenu :</strong> désigne l'ensemble des données (textes, images, audio, vidéo, construction sur les serveurs) publiés par Vultanium et/ou présentent sur l'un des Services.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">02. Objet</h3>
			<p class="text-part">Les présentes Conditions Générales de Vente ont pour objet de définir les termes et conditions dans lesquelles Vultanium vend et livre des produits (biens immatériels) aux Utilisateurs. Toute achat d'un produit sur l'un de nos Services se fera dans le respect des présentes Conditions Générales de Vente.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">03. Acceptation des presentes conditions generales de vente</h3>
			<p class="text-part">L'Utilisateur, déclare avoir pris connaissance et avoir accepté expressément et de manière inconditionnelle les présentes Conditions Générales de Vente en vigueur au jour de l'accès à sa boutique et à la souscription aux Services offerts par Vultanium. 
			Vultanium se réserve le droit de modifier tout ou partie et à tout moment les présentes Conditions Générales de Vente. Il appartient en conséquence à l'Utilisateur de se référer régulièrement à la dernière version des Conditions Générales de Vente disponible en permanence sur le Site Vultanium. Tout usage des Services après modification des Conditions Générales de Vente, vaut acceptation pure et simple par l'Utilisateur des nouvelles Conditions Générales de Vente.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">04. Produits</h3>
			<p class="text-part">Les caractéristiques des produits et services proposés à la vente sont présentées sur notre boutique <a href="boutique.php">http:/vultanium.fr/boutique</a></p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">05. Prix</h3>
			<p class="text-part">Les prix de nos articles sont indiqués sur la boutique au moment du paiement.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">06. Commande</h3>
			<p class="text-part">Toute commande validée par un clic ainsi que la saisie de données sur pages écrans successives, la saisie de votre numéro de carte bancaire et sa date d’expiration sur les sites de payement sécurisé : Paypal et autres... constituent une acceptation irrévocable des conditions générales de vente et du paiement et validera votre transaction. Les données enregistrées par Vultanium constituent la preuve de l’ensemble des transactions passées par Vultanium et ses clients. Les données enregistrées par le système de paiement constituent la preuve des transactions financières.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">07. Paiement</h3>
			<p class="text-part">Le paiement s’effectue en ligne par le biais de services partenaires. 
			Les informations personnelles ainsi que les numéros de carte bancaire ne passent jamais en clair entre le navigateur et le serveur, les données sont signées et cryptées.</p>
			<p>Au moment où l'utilisateur final souhaite effectuer un paiement, ses modalités d'inscription peuvent être transmises au partenaire concerné (Paypal) qui fournit des services de traitement des paiements pour mieux protéger les services et les utilisateurs finaux de paiements non autorisés.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">08. Frais de livraison</h3>
			<p class="text-part">Vous n'avez aucun frais de livraison à payer puisque tous les produits vendu sont des biens immatériels.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">09. Livraison</h3>
			<p class="text-part">La livraison est effectué lorsque le service de paiement utilisé nous indique que votre paiement a été validé. Votre service ensuite vous sera livré sous 15 minutes maximum. Plus d'informations sur l'accueil de notre boutique <a href="boutique.php"></a> http://vultanium.fr/boutique</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">10. Droit applicable</h3>
			<p class="text-part">Le présent contrat est soumis à la loi française. La langue du contrat est la langue française. Tout litige survenu dans le cadre de l'application ou de l'interprétation du présent contrat sera soumis aux tribunaux français compétents.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">11. Modalités de remboursement</h3>
			<p class="text-part">Vultanium livre des produits irrévocables non-réelles, nous ne publions pas de remboursements.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">12. Droit de rétraction</h3>
			<p class="text-part">Par dérogation à l'article L.121-20-2 du Code Français de la Consommation, le Client n'a plus de droit de rétractation à compter de la date et heure de fourniture de service. Il est important de noter que le service est mis en place instantanément en cas de paiement par carte bancaire, sms ou audiotel.</p>
			<br>
			<p>Par dérogation à l'article L.121-20-1 du Code Français de la Consommation, le Client a un droit de rétractation de 7 (sept) jours, si le service n'a pas encore été livré.</p>
			<br>
			<p>Ce droit de rétractation s'effectue par message à l'adresse email vultanium@gmail.com , et donne droit pour le client au remboursement des sommes déjà versées par lui dans un délai de trente (30) jours à compter de la réception de l'avis. Toute demande de rétractation qui ne respecterait pas le délai légal ou ne fournirait pas assez d'informations ne sera pas prise en considération.</p>
		</div>

	</div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>