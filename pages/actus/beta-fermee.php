<?php session_start(); ?>
<!-- Head/Navbar -->
<?php include('../../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Bêta fermée</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Content Accueil -->
<section class="section pattern-p1">
	<div class="container">
		<div class="col-lg-12">
			<a href="../../accueil.php" class="btn btn-success btn-back">« Retour</a>
			<h4 class="title">Êtes-vous prêt pour la bêta fermée ?</h4>
		</div>

		<img src="../../assets/img/beta-date.png" class="img-responsive" style="padding:2%;">

		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<p>» Vous l'attendez depuis longtemps, nous aussi, la voici!<br>
				La date et les inscriptions pour notre première bêta fermé sont tombée !</p>
				<hr>
				<p>Notre première bêta fermée aura lieu du :</p>
				<p><b>26 janvier 2019 au 10 février 2019</b></p>
				<hr>
				<p>Pour s'inscrire » <a href="#">Cliquer ici !</a> <span style="color: red;">(Inscription complète)</span></p>
			</div>
		</div>
	</div>
</section>

<!-- Footer -->
<?php include('../../inc/footer.php'); ?>