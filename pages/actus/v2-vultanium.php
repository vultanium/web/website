<?php session_start(); ?>
<!-- Head/Navbar -->
<?php include('../../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Saison 2 en approche</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Content Accueil -->
<section class="section pattern-p1">
	<div class="container">
		<div class="col-lg-12">
			<a href="../../accueil.php" class="btn btn-success btn-back">« Retour</a>
			<h4 class="title">Que nous réserve la saison 2 de Vultanium ?</h4>
		</div>

		<img src="../../assets/img/actusv2.png" class="img-responsive" style="padding:2%;">

		<div class="row">
			<div class="col-lg-12" style="text-align: center;">
				<p>» La saison 2 de Vultanium (une des plus importante), approche à petits pas... Quelles sont les nouveautés de cette saison ?!<br>
				Cette maintenance apporte de grand changements au serveur Vultanium. Le serveur sera uniquement un SemiRP mais pas n'importe quel SemiRP.</p>
				<p>Cette saison, 3 thèmes seront mis à l'honneur avec en plus des items ajouté et customisé par Vultanium. Vous pourrez découvrir :</p>
				<ul style="list-style: none;">
					<li><b>» Projet Titan :</b> Ce monde vous guidera dans les dimensions</li>
					<li><b>» Astrémis :</b> Une belle ville médiévale en bord de mer</li>
					<li><b>» Tsukimi :</b> Une ville Japonaise cachant de grand secrets</li>
				</ul>
				<p>Un Ressource Pack vous sera ajouté automatiquement lors de votre connexion au serveur afin que vous puissiez profiter d'items custom que vous trouverez nulle part ailleurs sur Minecraft !<br>
				Il ne vous reste plus qu'à patienter avant de découvrir la plus belle saison de Vultanium !</p>
			</div>
		</div>
	</div>
</section>

<!-- Footer -->
<?php include('../../inc/footer.php'); ?>