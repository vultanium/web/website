<!-- Header / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Notre boutique</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Content Boutique -->
<section class="section pattern-p1">
	<div class="container">

		<h4 class="title" style="text-align: center;" data-aos="fade-down" data-aos-duration="2000">Bienvenue sur la boutique de Vultanium</h4>
		<p class="text-launcher" data-aos="fade-down" data-aos-duration="2000">Retrouver nos grades, pass, cosmétics et bien d'autres...</p>

		<hr>

		<div class="row">

			<div class="col-md-9">

				<div class="row">

					<div class="col-md-3">
						<div class="card h-100">
							<a href="#"><img class="card-img-top" src="http://placehold.it/400x500" alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Grade Virtuose</a>
								</h4>
								<h5 class="price">2.99€ / mois</h5>
								<p class="card-text">Description produit</p>
							</div>
							<a href="#"><div class="card-footer btn-buy" data-toggle="modal" data-target="#modalVirtuose">
								<i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter
							</div></a>
						</div>
					</div>

						<!-- ------------------------------------------- -->

					<div class="col-md-3">
						<div class="card h-100">
							<a href="#"><img class="card-img-top" src="http://placehold.it/400x500" alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Grade Renégat</a>
								</h4>
								<h5 class="price">6.99€ / mois</h5>
								<p class="card-text">Description produit</p>
							</div>
							<a href="#"><div class="card-footer btn-buy" data-toggle="modal" data-target="#modalRenegat">
								<i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter
							</div></a>
						</div>
					</div>

						<!-- ------------------------------------------- -->

					<div class="col-md-3">
						<div class="card h-100">
							<a href="#"><img class="card-img-top" src="http://placehold.it/400x500" alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Grade Supernova</a>
								</h4>
								<h5 class="price">9.99€ / mois</h5>
								<p class="card-text">Description produit</p>
							</div>
							<a href="#"><div class="card-footer btn-buy" data-toggle="modal" data-target="#modalSupernova">
								<i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter
							</div></a>
						</div>
					</div>

						<!-- ------------------------------------------- -->

					<div class="col-md-3">
						<div class="card h-100">
							<a href="#"><img class="card-img-top" src="http://placehold.it/400x500" alt=""></a>
							<div class="card-body">
								<h4 class="card-title">
									<a href="#">Pass Suprême</a>
								</h4>
								<h5 class="price">4.50€ / saison</h5>
								<p class="card-text" >Description produit</p>
							</div>
							<a href="#"><div class="card-footer btn-buy" data-toggle="modal" data-target="#modalPass">
								<i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter
							</div></a>
						</div>
					</div>

				</div>

			</div>


			<div class="col-md-3">
				<h4 class="title-shop">Informations</h4>
				<input class="disabled shop-pseudo" value="Mon pseudo" />
				<a href="#" class="btn btn-panier"><i class="fas fa-shopping-basket" style="color:#fff;"></i> Mon panier</a>
			</div>

		</div>

	</div>
</section>

<!-- Modals -->
<?php include('../modules/modals.php'); ?>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>