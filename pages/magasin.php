<?php session_start(); ?>
<!-- Header / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="header">
	<div class="container">
		<div class="content-header" data-aos="fade-down">
			<p href="#" class="connect-serveur">Notre magasin</p>
		</div>
	</div>
</header>

<!-- Content Magasin -->
<section class="section pattern-p1">
	<div class="container">
		<div id="ShopVulta">
		    <a href="https://shop.spreadshirt.fr/vultanium"></a>
		</div>
	</div>
</section>

<!-- Spreadshirt -->
<script type="text/javascript" src="../../assets/js/spreadshirt.js"></script>
<script type="text/javascript" src="https://shop.spreadshirt.fr/shopfiles/shopclient/shopclient.nocache.js"></script>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>