<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- 404 -->
<header class="header-404 color_overlay d-flex align-items-center parallax-error">
    <div class="container">
        <div class="wrapper-404-alert"
             data-aos="flip-right"
             data-aos-delay="700"
             data-aos-easing="ease-in"
             data-aos-duration="600">
            <div class="content-error">
                <h2 class="display-2 title-error">Erreur 404</h2>
                <h2>Page non trouvé</h2>
                <p>Aucune page portant ce nom existe sur notre site.</p>
                <div class="d-flex justify-content-center">
                    <a href="../accueil.php" class="button-back"><i class="fa fa-angle-left"></i>Retour à l'accueil</a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
