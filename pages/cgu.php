<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">CGU</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Conditions générales d'utilisation -->
<section class="section pattern-p1">
	<div class="container">
	<h4 class="title">Conditions générales d'utilisation</h4>

		<div class="post-content" style="margin-top: 50px;">
			<h3 class="post-title reglement-title">01. Définitions</h3>
			<p class="text-part"><strong>Services :</strong> désigne l'ensemble des services offerts par Vultanium par l'intermédiaire du site <a href="http://vultanium.fr">http://vultanium.fr</a>, des serveurs de jeux Minecraft et de son serveur de communication vocal Discord. (<a href="https://discord.gg/bUSvAev" target="_blank">http://discord.vultanium.fr</a>)</p>
			<p><strong>Utilisateur :</strong> désigne la personne physique, utilisant les Services offerts par Vultanium.</p>
			<p><strong>Données Personnelles :</strong> désigne les informations personnelles que l'Utilisateur a enregistrées lors de son inscription aux Services proposés par Vultanium et/ou fournies dans le cadre de l'utilisation des Services.</p>
			<p><strong>Droits de Propriété Intellectuelle :</strong> désignent les marques, les noms de domaine, les droits d'auteur, les copyrights, les dessins, les brevets, les droits sur les Bases de Données ou tous autres droits de propriété intellectuelle exploités par Vultanium et nécessaires à ses activités de prestataire de Services.</p>
			<p><strong>Formulaire :</strong> désigne le formulaire d'inscription ou de demande d'informations que l'Utilisateur peut compléter pour pouvoir bénéficier de certains services de Vultanium après avoir pris connaissance des présentes Conditions Générales d'Utilisation.</p>
			<p><strong>Lien Hypertexte :</strong> désigne le système de référencement matérialisé par un mot, une icône ou un logo qui permet par un click de souris de passer d'un document à un autre sur un même site web ou d'une page d'un site web à la page d'un autre site web.</p>
			<p><strong>Site Web :</strong> désigne le site Internet mis à la disposition du public et des Utilisateurs.</p>
			<p><strong>Compte :</strong> désigne les données qui représentent l'Utilisateur sur les Services. Tels que le pseudonyme, l'email, le mot de passe, ...</p>
			<p><strong>Contenu :</strong> désigne l'ensemble des données (textes, images, audio, vidéo, construction sur les serveurs) publiés par Vultanium et/ou présentent sur l'un des Services.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">02. Objet</h3>
			<p class="text-part">Les présentes Conditions Générales d'Utilisation ont pour objet de définir les termes et conditions dans lesquelles Vultanium fournit ses Services aux Utilisateurs. Toute utilisation de l'un des Services offerts par Vultanium se fera dans le respect des présentes Conditions Générales d'Utilisation.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">03. Acceptation des présentes conditions générales d'utilisation</h3>
			<p class="text-part">L'Utilisateur, déclare avoir pris connaissance et avoir accepté expressément et de manière inconditionnelle les présentes Conditions Générales d'Utilisation en vigueur au moment de son accès aux Services offerts par Vultanium.</p> 
			<p>Dans le cas ou l'utilisateur n'accepte pas les présentes Conditions d'Utilisation il s'engage à ne pas utiliser les Services offerts par Vultanium.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">04. Droits de propriété intellectuelle</h3>
			<p class="text-part">L'Utilisateur reconnaît que Vultanium est avec MOJANG seul propriétaire des Droits de Propriété Intellectuelle présents sur l'ensemble des services fournis. 
			Aucune disposition des présentes Conditions Générales d'Utilisation ne pourra être interprétée comme conférant à l'Utilisateur une licence sur les Droits de Propriété Intellectuelle, dont Vultanium pourra avoir la propriété ou le droit exclusif d'exploitation. 
			Vultanium ne concède qu'une autorisation de visualisation de son Contenu à titre personnel et privé, à l'exclusion de toute visualisation ou diffusion publique.</p>
			<p>Par conséquent, toute représentation, reproduction ou extraction non autorisée par Vultanium du Contenu ou de son site web ainsi que tout autre Droit de Propriété Intellectuelle est strictement interdite sous peine de poursuites judiciaires. Le contrevenant s'expose à des sanctions civiles et pénales et notamment aux peines prévues aux articles L. 335.2 et L. 343.1 du Code de la Propriété Intellectuelle.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">05. Responsabilité</h3>
			<p class="text-part">L'Utilisateur reconnaît expressément qu'il doit faire preuve de discernement dans l'utilisation du Contenu et supporter tous les risques y afférents, et notamment lorsqu'il se fie à l'opportunité, l'utilité ou le caractère complet de ce Conten, et procéder à toute vérification sous sa responsabilité. 
			L'équipe Vultanium apporte tout le soin qu'elle estime nécessaire à la constitution du Contenu. Toutefois Vultanium n'assure aucune garantie quant à la fiabilité du Contenu. A ce titre, Vultanium ne saurait être tenue responsable des dommages résultant de l'utilisation par l'Utilisateur à quelque fin que ce soit, du Contenu.</p>
			<p>Les Services proposés contiennent des Liens Hypertextes vers des sites web gérés par des tiers. Vultanium ne peut exercer aucun contrôle sur ces sites ni assumer aucune responsabilité quant à leur contenu. L'insertion de ces liens ne signifie pas que Vultanium approuve les éléments contenus sur ces sites. Vultanium ne peut être tenue responsable du contenu de ces sites, et le fait que ces sites soient référencés sur les Services proposés n'engage en aucun cas la responsabilité de Vultanium. </p>
			<br>
			<p><strong>Vultanium ne donne donc aucune garantie concernant : </strong></p>
			<ul>
				<li>- La véracité, l'actualité, la qualité, la complétude et l'exhaustivité du contenu des sites Web indexés ;</li>
				<li>- La pertinence et l'exhaustivité des sites web indexés ; </li>
				<li>- Les difficultés d'accès et de fonctionnement de ces sites web.</li>
			</ul>
			<br>
			<p><strong>L'Utilisateur reconnaît que :</strong></p>
			<ul>
				<li>- L'utilisation des Services se fait à ses risques et périls notamment en ce qui concerne le téléchargement de données, de fichiers ou de logiciels qui pourraient endommager son ordinateur ou le support utilisé par l'Utilisateur. La responsabilité de Vultanium ne peut en particulier être engagée pour toute perte de données, virus, bogues informatiques ou dommage affectant son ordinateur et/ou ses données personnelles et/ou les données de son Compte ;</li>
				<li>- Vultanium ne garantit pas l'adéquation entre les Services Proposés et les attentes de l'Utilisateur;</li>
				<li>- Vultanium ne garantit pas la qualité et/ou la licéité ou la conformité à la loi du contenu non créé par Vultanium notamment du contenu et des opinions émises par l'Utilisateur ou diffusés sur les sites extérieurs accessibles par le biais d'un Lien Hypertexte ;</li>
				<li>- Vultanium ne saurait être tenue responsable de la diffusion des données par l'Utilisateur en violation de droits, notamment privatifs, détenus par des tiers ;</li>
				<li>- Vultanium ne saurait être tenue pour responsable en cas de dommage résultant de causes techniques et notamment de l'indisponibilité de ses Services et de tout défaut affectant le fonctionnement de ceux-ci ;</li>
				<li>- Vultanium ne pourra être tenue responsable de dommages directs ou indirects tels que pertes financières, manque à gagner, trouble de quelque nature que ce soit qui pourraient résulter de l'utilisation ou de l'impossibilité d'utilisation des Services proposés.</li>
			</ul>
			<br>
			<p>En outre, l'Utilisateur garantit Vultanium contre tout recours ou action que pourrait lui intenter à titre quelconque et pour quelque raison que ce soit, et plus généralement à l'occasion de la saisie de données ou informations, toute personne qui s'estimerait lésée par la mise en ligne de ces données ou informations sur les Services et s'engagent à la tenir indemne contre tous frais judiciaires et extra judiciaires qui pourraient en résulter.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">06. Acceptation des risque sur internet</h3>
			<p class="text-part"><strong>L'Utilisateur déclare bien connaître Internet, ses caractéristiques et ses limites et en particulier reconnaît :</strong></p>
			<ul>
				<li>- Que l'Internet est un réseau ouvert non maîtrisable par Vultanium et que les échanges de données circulant sur Internet ne bénéficient que d'une fiabilité relative, et ne sont protégées notamment contre les risques de détournements ou de piratages éventuels ; </li>
				<li>- Que la communication par l'Utilisateur d'informations à caractère sensible est donc effectuée à ses risques et périls ;</li>
				<li>- Avoir connaissance de la nature du réseau Internet et en particulier de ses performances techniques et des temps de réponse, pour consulter, interroger ou transférer les données d'informations.</li>
			</ul>
			<br>
			<p>Vultanium ne peut garantir que les informations échangées ne seront pas interceptées par des tiers, et que la confidentialité des échanges sera garantie. 
			Vultanium informe l'Utilisateur de l'existence de règles et d'usage en vigueur sur Internet connus sous le nom de Netiquette ainsi que de différents codes de déontologie et notamment la Charte Internet accessibles sur Internet.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">07. Accessibilité aux services</h3>
			<p class="text-part">Vultanium s'efforce dans la mesure du possible de maintenir accessible ses Services 7 jours sur 7 et 24 heures sur 24 mais peut interrompre l'accès, notamment pour des raisons de maintenance et de mises à niveau ou pour toute autre raison notamment technique. Vultanium n'est en aucun cas responsable de ces interruptions et des conséquences qui peuvent en découler pour l'Utilisateur. 
			Vultanium se réserve le droit de refuser à tout Utilisateur l'accès à tout ou partie des Services unilatéralement et sans notification préalable, notamment en cas de violation manifeste des présentes Conditions Générales d'Utilisation. 
			L'Utilisateur reconnaît que Vultanium ne pourra être tenue responsable de tout dommage direct ou indirect survenu à raison de la suppression de l'accès de l'Utilisateur aux Services.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">08. Conditions d'accès aux services</h3>
			<p class="text-part">L'accès aux Services gratuits est totalement libre.</p>
			<br>
			<p>L'utilisation des Services Spécifiques suppose le respect par l'Utilisateur d'une procédure d'inscription par laquelle ce dernier doit founir une adresse mail. L'Utilisateur s'engage à ce que les informations communiquées, soient exactes, complètes et à jour et à effectuer les modifications nécessaires à cette fin.</p>
			<p>En contre partie de la fourniture gratuite de services spécifiques tels que l'accès au forum, le changement de mot de passe, vous acceptez, en tant qu'UTILISATEUR, de recevoir au maximum trois fois par semaine, une offre commerciale par email expédiée par Vultanium ou ses partenaires. 
			A tout moment, vous pouvez vous opposer à la réception de ce type d'offres. Chaque message publicitaire contient un lien permettant l'arrêt des envois et votre désinscription.</p>
			<p>Dans le cadre de cette procédure, l'Utilisateur déclare avoir pris connaissance et avoir accepté expressément les présentes Conditions Générales d'Utilisation en vigueur au jour de la souscription aux Services Spécifiques par un clic sur l'icône intitulée "J'accepte les Conditions Générales d'Utilisation". Toute acceptation exprimée par l'Utilisateur par un clic vaut signature au même titre que sa signature manuscrite. Par la réalisation de ce clic l'Utilisateur est réputé avoir donné son accord irrévocablement.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">09. Données personnelles</h3>
			<p class="text-part" style="text-transform: uppercase;"><strong>9.1 : Confidentialité des données recueillies</strong></p><br>
			<p><strong>Vultanium collecte des informations fournies par les Utilisateurs : </strong></p>
			<ul>
				<li>- Par le biais d'un Formulaire ou directement envoyées par les Utilisateurs pour mieux les connaître et les faire bénéficier des Services Spécifiques de Vultanium ; </li>
				<li>- Par le biais de commandes executés par l'utilisateur sur l'un des services proposé par Vultanium.</li>
			</ul>
			<br>
			<p>Les informations recueillies font l’objet d’un traitement informatique destiné à améliorer la qualité des services, protéger l'utilisateur et éviter les comportements interdits. Les destinataires des données sont : les administateurs et les techniciens de Vultanium. 
			Conformément à la loi « informatique et libertés » du 6 janvier 1978 modifiée en 2004, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent, que vous pouvez exercer en vous adressant au service client de Vultanium depuis le formulaire de contact. 
			Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.</p>
			<br>
			<p class="text-part" style="text-transform: uppercase;"><strong>9.2 : Utilisation des cookies</strong></p><br>
			<p>Nous utilisons des cookies persistant afin d'améliorer l'expérience utilisateur. Ces cookies sont conservés sur votre ordinateur même après fermeture de votre navigateur et réutilisé lors des prochaines visites sur nos sites. Nous utilisons aussi des cookies afin de mieux comprendre comment vous interagissez avec nos sites et nos services et afin d'améliorer ces mêmes sites et services.
			Google en tant que vendeur tiers, utilise des cookies pour diffuser de la publicité sur nos sites. L'utilisation par Google de cookies DART permet de diffuser de la publicité aux internautes sur nos site et des sites tiers.</p>
		</div>

	</div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
