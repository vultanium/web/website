<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Partenaires</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<!-- Partenaires -->
<section class="section pattern-p1">
    <div class="container">
        <h4 class="title">Page en cours d'édition...</h4>
        <p style="margin-bottom:40px;">Elle sera accessible très prochainement...</p>
    </div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
