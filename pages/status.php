<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Status des services</span>
			  </span>
			</h1>
		</div>
	</div>
</header>

<section class="section pattern-p1">
    <div class="container">
        <iframe src="https://stats.uptimerobot.com/M1X8GiMxL" frameborder="0" style="width: 100%;height: 1040px;"></iframe>
    </div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
