<?php session_start(); ?>
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
	<div class="container">
		<div class="pages-header">
			<h1 class="ml6">
			  <span class="text-wrapper">
			    <span class="letters">Règlement</span>
			  </span>
			</h1>
		</div>
	</div>
</header>


<!-- Règlement -->
<section class="section pattern-p1">
	<div class="container">

		<div class="post-content" style="margin-top: 15px;">
			<h3 class="post-title reglement-title">01. Les pseudonymes</h3>
			<p class="text-part">Il est interdit d'utiliser des pseudonymes à caractères raciste, religieux, haineux, etc... sous peine d'un bannissement de nos serveurs, jusqu’a ce que vous le changiez en un autre plus convenable.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">02. Spam et flood</h3>
			<p class="text-part">Il est interdit de spam plusieurs caractères, plusieurs mots ou plusieurs messages dans le chat. Il est également interdit de flood. Si vous ne respectez pas cette règle, deux avertissement vous seront donnés, ainsi qu'un mute de 15 minutes. Si vous récidivez, vous aurez un kick et un bannissement temporaire de 3 jours et si vous récidivez encore, vous aurez un bannissement temporaire de 7 jours.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">03. Pub</h3>
			<p class="text-part">Toutes pub pour des liens Youtube, serveurs Minecraft, pages Facebook, serveurs Discord et autres sont strictement interdites. Si cette règle n'est pas respectée, un mute de 30 minutes vous sera donné, puis un kick, ainsi qu'un bannissement de 7 jours si vous récidivez.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">04. Comportement</h3>
			<p class="text-part">Il est important de rester un minimum poli, éviter le langage SMS, ne pas manquer de respect à n'importe quel joueur et membre du staff, ne pas insulter, ne pas provoquer à répétition. Tout manquement sera sanctionné d'un kick et d'un ban de 7 jours, puis un ban définitif si cela se reproduit. Le staff à toujours raison. Si vous rencontrez un problème avec un autre joueur, ne ne le réglez pas seul, contactez un modérateur ou un administrateur. Vous pouvez effectuer un report en jeu avec la commande /report (pseudo) ou sur notre Discord dans le salon #report-joueur</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">05. Cheat et compagnie</h3>
			<p class="text-part">Il est strictement interdit d'utiliser toutes sortes de logiciels pour tricher sur nos serveurs. Le cheat (hacks pvp, speed hacks, xray, grief et autres...) est sanctionné directement par un ban de 7 jours puis un ban définitif si vous récidivez.<br>
			Si vous êtes surpris en train de tricher, un modérateur ou un administrateur est dans l'autorisation de vous convoquer sur Discord pour discuter (Faute à moitié avouée, faute à moitié pardonnée). Si vous refusez cet entretrien, le membre du staff est dans l'autorisation de vous bannir 7 jours la première fois puis définitivement si vous récidivez.</p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<h3 class="post-title reglement-title">06. Conditions d'utilisation</h3>
			<p class="text-part">Merci de prendre connaissances et de tenir compte des conditions générales d'utilisations à cette adresse : <a href="http://vultanium.fr/pages/cgu.php">http://vultanium.fr/cgu</a></p>
		</div>

		<div class="post-content" style="margin-top: 80px;">
			<p class="text-part"><strong>L'équipe Vultanium à l'autorisation de changer ce règlement si nécéssaire, le staff est dans l'autorisation de vous sanctionner si celui-ci n'est pas respecté. Le staff ayant toujours raison.</strong></p>
		</div>

	</div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>