<?php

$prenom = $_POST['prenom'];
$pseudo = $_POST['pseudo'];
$email = $_POST['email'];
$age = $_POST['age'];
$centres = $_POST['centres'];
$horaires = $_POST['horaires'];
$discord = $_POST['discord'];
$premium = $_POST['premium'];
$minecraft = $_POST['minecraft'];
$decouverte = $_POST['decouverte'];
$attirance = $_POST['attirance'];
$poste = $_POST['poste'];
$mature = $_POST['mature'];
$motivation = $_POST['motivation'];

$destinataire = "vultanium@gmail.com, retro@vultanium.fr, leo@vultanium.fr, neoztoxic@vultanium.fr";
$headers = "Vous avez reçu une candidature de $prenom pour le poste : $poste";
$body = "
PRENOM : $prenom \r\n
PSEUDO MC : $pseudo \r\n
EMAIL : $email \r\n
AGE : $age \r\n

» Postule pour le poste de $poste \r\n

• Centre d'intérêts : \r\n
$centres

\r\n
• Horaires de connexion : \r\n
$horaires

\r\n
PSEUDO DISCORD : $discord \r\n
MINECRAFT PREMIUM : $premium \r\n

• Pourquoi je joue à Minecraft : \r\n
$minecraft
\r\n

• Comment j'ai découvert le serveur : \r\n
$decouverte
\r\n

• Qu'est-ce qui m'attire sur Vultanium : \r\n
$attirance
\r\n

• Suis-je assez mature pour ce poste : \r\n
$mature
\r\n

• Mes motivations : \r\n
$motivation
";

if (!empty($_POST)) {
    if ($captcha->isValid($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']) === false) {?>
        <div class="alert alert-danger" style="width: 60%;margin: auto;display: block;padding: 1%;margin-bottom: 2%;">Le CAPTCHA est invalide. Veuillez le cocher.</div>
<?php } else {
		if (mail($destinataire,$headers,$body)) {	
			header('Location: recrutement.php');
		} ?>
        <div class="alert alert-success" style="width: 60%;margin: auto;display: block;padding: 1%;margin-bottom: 2%;">Votre message à bien été envoyé. Nous vous remercions.</div>
    <?php }
}
?>