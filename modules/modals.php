<!-- Grade Virtuose -->
<div class="modal fade" id="modalVirtuose" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Grade Virtuose - 2.99€/MOIS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Description du grade...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-panier-confirm"><i class="fas fa-shopping-basket" style="color:#fff;"></i> Ajouter au panier</button>
        <button type="button" class="btn btn-buy-confirm"><i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter</button>
      </div>
    </div>
  </div>
</div>

<!-- Grade Renegat -->
<div class="modal fade" id="modalRenegat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Grade Renegat - 6.99€/MOIS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Description du grade...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-panier-confirm"><i class="fas fa-shopping-basket" style="color:#fff;"></i> Ajouter au panier</button>
        <button type="button" class="btn btn-buy-confirm"><i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter</button>
      </div>
    </div>
  </div>
</div>

<!-- Grade Supernova -->
<div class="modal fade" id="modalSupernova" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Grade Supernova - 9.99€/MOIS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Description du grade...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-panier-confirm"><i class="fas fa-shopping-basket" style="color:#fff;"></i> Ajouter au panier</button>
        <button type="button" class="btn btn-buy-confirm"><i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter</button>
      </div>
    </div>
  </div>
</div>

<!-- Pass -->
<div class="modal fade" id="modalPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 8%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pass Suprême - 4.50€/SAISON</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Description du pass...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-panier-confirm"><i class="fas fa-shopping-basket" style="color:#fff;"></i> Ajouter au panier</button>
        <button type="button" class="btn btn-buy-confirm"><i class="fas fa-shopping-cart" style="color:#fff;"></i> Acheter</button>
      </div>
    </div>
  </div>
</div>