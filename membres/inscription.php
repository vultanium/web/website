<?php
session_start();
require_once '../inc/functions.php';
if(!empty($_POST)){

    $errors = array();
    require_once '../inc/config.php';

    if(empty($_POST['username'])){
        $errors['username'] = "Votre nom et prénom contient des caractères non pris en compte";
    } else {
        $req = $pdo->prepare('SELECT id FROM users WHERE username = ?');
        $req->execute([$_POST['username']]);
        $user = $req->fetch();
        if($user){
            $errors['username'] = 'Un compte à ce nom existe déjà';
        }
    }

    if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Votre email n'est pas valide";
    } else {
        $req = $pdo->prepare('SELECT id FROM users WHERE email = ?');
        $req->execute([$_POST['email']]);
        $user = $req->fetch();
        if($user){
            $errors['email'] = 'Cet email est déjà utilisé pour un autre compte';
        }
    }

    if(empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']){
        $errors['password'] = "Vous devez rentrer un mot de passe valide";
    }

if(empty($errors)){

    $req = $pdo->prepare("INSERT INTO users SET username = ?, password = ?, email = ?, confirmation_token = ?");
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $token = str_random(60);
    $req->execute([$_POST['username'], $password, $_POST['email'], $token]);
    $user_id = $pdo->lastInsertId();
    mail($_POST['email'], 'Confirmation de votre compte sur Vultanium', "Afin de valider votre compte merci de cliquer sur ce lien\n\nhttp://vultanium.fr/membres/confirm.php?id=$user_id&token=$token");
    $_SESSION['flash']['success'] = 'Merci pour votre inscription. Un email de confirmation vous a été envoyé pour valider votre compte. Vous devez valider votre compte pour vous connecter';
    header('Location: connexion.php');
    exit();
}


}
?>

<!-- Head / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
    <div class="container">
        <div class="pages-header">
            <h1 class="ml6">
              <span class="text-wrapper">
                <span class="letters">Bienvenue parmis nous</span>
              </span>
            </h1>
        </div>
    </div>
</header>

<!-- Content Inscription -->
<section class="section pattern-p1" data-aos="fade-up">
    <div class="form-content">

      <?php if(!empty($errors)): ?>
      <div class="alert alert-danger">
          <p>Vous n'avez pas rempli le formulaire correctement</p>
          <ul>
              <?php foreach($errors as $error): ?>
                 <li><?= $error; ?></li>
              <?php endforeach; ?>
          </ul>
      </div>
      <?php endif; ?>

      <div class="container">
        <?php if(isset($_SESSION['flash'])): ?>
            <?php foreach($_SESSION['flash'] as $type => $message): ?>
                <div class="alert alert-<?= $type; ?>">
                    <?= $message; ?>
                </div>
            <?php endforeach; ?>
            <?php unset($_SESSION['flash']); ?>
        <?php endif; ?>
      </div>

    	<!-- Formulaire d'inscription -->
    	<form action="" method="POST" class="form-members">
    		<div class="form-group">
    			<label for="">Pseudo Minecraft</label>
    			<input type="text" name="username" class="form-control">
    		</div>
    		<div class="form-group">
    			<label for="">Mail</label>
    			<input type="email" name="email" class="form-control">
    		</div>
    		<div class="form-group">
    			<label for="">Mot de passe</label>
    			<input type="password" name="password" class="form-control">
    		</div>
    		<div class="form-group">
    			<label for="">Confirmer Mot de passe</label>
    			<input type="password" name="password_confirm" class="form-control">
    		</div>
            <div class="form-group">
                <label></label>
                <input type="checkbox" name="" value="" required> J'ai lu et j'accepte les <a href="../pages/cgu.php">Conditions Générales d'Utilisation</a> / <a href="../pages/cgv.php">Conditions Générales de Vente</a> ainsi que la Politique de Protection des Données Personnelles. 
            </div>

            <div class="btns-form">
                <input type="submit" class="btn btn-success btn-inscription" value="S'inscrire" />
                <a class="btn btn-success btn-already-account" href="connexion.php">J'ai déjà un compte</a>
            </div>
    	</form>
    </div>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>