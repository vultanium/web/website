<?php

session_start();
require_once '../inc/functions.php';
reconnect_from_cookie();

if (isset($_SESSION['auth'])) {
    header('Location: compte.php');
    exit();
}

if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])) {
    require_once '../inc/config.php';

    $req = $pdo->prepare('SELECT * FROM users WHERE (username = :username OR email = :username) AND confirmed_at IS NOT NULL');
    $req->execute(['username' => $_POST['username']]);
    $user = $req->fetch();

    if (password_verify($_POST['password'], $user->password)) {
        $_SESSION['auth'] = $user;
        $_SESSION['flash']['success'] = 'Vous êtes maintenant connecté';

        if ($_POST['renember']) {
            $renember_token = str_random(250);
            $pdo->prepare('UPDATE users SET renember_token = ? WHERE id = ?')->execute([$renember_token, $user->id]);

            setcookie('renember', $user->id . '==' . $renember_token . sha1($user->id . 'ratonlaveurs'), time() + 60 * 60 * 24 * 7);
        }

        header('Location: compte.php');
        exit();
    } else {
        $_SESSION['flash']['danger'] = "Email ou mot de passe incorrect ou vous n'avez pas validé votre compte par email";
    }
}
?>

<!-- Head / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="parallax">
    <div class="container">
        <div class="pages-header">
            <h1 class="ml6">
              <span class="text-wrapper">
                <span class="letters">Ravis de vous revoir</span>
              </span>
            </h1>
        </div>
    </div>
</header>

<!-- Content Connexion -->
<section class="section pattern-p1">

  <div class="container">
    <?php if(isset($_SESSION['flash'])): ?>
        <?php foreach($_SESSION['flash'] as $type => $message): ?>
            <div class="alert alert-<?= $type; ?>">
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
    <?php endif; ?>
  </div>

	<!-- Formulaire d'inscription -->
	<form action="" method="POST" class="form-members">
		<div class="form-group">
			<label for="">Pseudo Minecraft ou votre email</label>
			<input type="text" name="username" class="form-control">
		</div>
		<div class="form-group">
			<label for="">Mot de passe <small><a href="reset_mdp.php">(Mot de passe oublié ?)</a></small></label>
			<input type="password" name="password" class="form-control">
		</div>
		<div class="form-group">
			<label></label>
			<input type="checkbox" name="renember" value="1"> Se souvenir de moi
		</div>

        <div class="btns-form">
    		<input type="submit" class="btn btn-success btn-inscription" value="Se connecter" />
            <a class="btn btn-success btn-already-account" href="inscription.php">Je n'ai pas de compte</a>
        </div>
	</form>

</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
