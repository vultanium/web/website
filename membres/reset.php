<?php
if(isset($_GET['id']) && isset($_GET['token'])){
    require '../inc/config.php';
    require '../inc/functions.php';
    $req = $pdo->prepare('SELECT * FROM users WHERE id = ? AND reset_token IS NOT NULL AND reset_token = ? AND reset_at > DATE_SUB(NOW(), INTERVAL 30 MINUTE)');
    $req->execute([$_GET['id'], $_GET['token']]);
    $user = $req->fetch();
    if($user){
        if(!empty($_POST)){
            if(!empty($_POST['password']) && $_POST['password'] == $_POST['password_confirm']){
                $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                $pdo->prepare('UPDATE users SET password = ?, reset_at = NULL, reset_token = NULL WHERE id = ?')->execute([$password, $_GET['id']]);
                session_start();
                $_SESSION['flash']['success'] = 'Votre mot de passe a bien été modifié';
                $_SESSION['auth'] = $user;
                header('Location: compte.php');
                exit();
            }
        }
    }else{
        session_start();
        $_SESSION['flash']['danger'] = "Ce token n'est pas valide";
        header('Location: connexion.php');
        exit();
    }
}else{
    header('Location: connexion.php');
    exit();
}
?>


<?php include('../inc/head.php'); ?>


<header class="header-404 color_overlay d-flex align-items-center"
        style="background-image: url('../assets/img/banner/header.png')">
    <div class="container">
        <div class="wrapper-404-alert">
            <div class="content-error">
                <h4 class="title-forget">Réinitialisation du mot de passe</h4>

                <form action="" method="POST">
                    <div class="form-group">
                        <label for="">Nouveau mot de passe</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Confirmer le nouveau mot de passe</label>
                        <input type="password" name="password_confirm" class="form-control">
                    </div>

                    <input type="submit" class="btn btn-success btn-inscription" style="width: 30%;" value="Réinitialiser mon mot de passe">
                </form>

            </div>
        </div>
    </div>
</header>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
