<?php
if(!empty($_POST) && !empty($_POST['email'])){
    require_once '../inc/config.php';
    require_once '../inc/functions.php';
    $req = $pdo->prepare('SELECT * FROM users WHERE email = ? AND confirmed_at IS NOT NULL');
    $req->execute([$_POST['email']]);
    $user = $req->fetch();
    if($user){
        session_start();
        $reset_token = str_random(60);
        $pdo->prepare('UPDATE users SET reset_token = ?, reset_at = NOW() WHERE id = ?')->execute([$reset_token, $user->id]);
        $_SESSION['flash']['success'] = 'Les instructions du rappel de mot de passe vous ont été envoyées par emails';
        mail($_POST['email'], 'Réinitiatilisation de votre mot de passe', "Afin de réinitialiser votre mot de passe merci de cliquer sur ce lien\n\nhttp://vultanium.fr/membres/reset.php?id={$user->id}&token=$reset_token");
        header('Location: ../membres/connexion.php');
        exit();
    }else{
        $_SESSION['flash']['danger'] = 'Aucun compte ne correspond à cet adresse mail';
    }
}
?>


<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="header-404 color_overlay d-flex align-items-center"
        style="background-image: url('../assets/img/banner/header.png')">
    <div class="container">
        <div class="wrapper-404-alert">
            <div class="content-error">
                <h4 class="title-forget">Oups ! J'ai oublié mon mot de passe..</h4>

                <form action="" method="POST">
                    <div class="form-group">
                        <label for="">Votre email</label>
                        <input type="email" name="email" class="form-control">
                    </div>

                    <input type="submit" class="btn btn-success btn-inscription" style="width: 30%;" value="Réinitialiser mon mot de passe">
                </form>

            </div>
        </div>
    </div>
</header>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
