<?php
session_start();
require '../inc/functions.php';
logged_only();

if (!empty($_POST)) {
    if (empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']) {
        $_SESSION['flash']['danger'] = "Les mots de passes ne correspondent pas";
    } else {
        $user_id = $_SESSION['auth']->id;
        $password= password_hash($_POST['password'], PASSWORD_BCRYPT);

        require_once '../inc/config.php';

        $pdo->prepare('UPDATE users SET password = ? WHERE id = ?')->execute([$password, $user_id]);
        $_SESSION['flash']['success'] = "Votre mot de passe a bien été mis à jour";
    }
}

if ($_GET['action'] == 'deleteme') {
    require_once '../inc/config.php';

    $pdo->prepare("DELETE FROM users WHERE id = ?")->execute(array($_SESSION['auth']->id)); 

    header("Location: deconnexion.php");
}
?>

<!-- Head / Navbar -->
<?php include('../inc/head.php'); ?>

<!-- Header -->
<header class="header">
    <div class="container">
        <div class="content-header">
            <h1 class="ml11 title-me">
                <span class="text-wrapper">
                    <span class="line line1"></span>
                    <span class="letters">Bienvenue sur votre espace membre <?= $_SESSION['auth']->username; ?></span>
                </span>
            </h1>
        </div>
    </div>
</header>

<!-- Content Membre -->
<section class="section pattern-p1">

  <div class="container">
    <?php if(isset($_SESSION['flash'])): ?>
        <?php foreach($_SESSION['flash'] as $type => $message): ?>
            <div class="alert alert-<?= $type; ?>">
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
    <?php endif; ?>
  </div>

    <!-- Espace membre -->
    <section class="blog-section">
        <div class="container-fluid blog-layout-sidebar-wrapper">
            <div class="row blog-layout-sidebar">

                <div class="col-md-8">

                    <!-- Formulaire information -->
                    <div class="col-lg-12">
                        <form action="" method="POST">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Votre pseudo Minecraft</label>
                                    <input class="form-control compte-form" type="text" placeholder="<?= $_SESSION['auth']->username; ?>" disabled>
                                </div>

                                <div class="form-group col-lg-6">
                                    <label>Votre adresse mail</label>
                                    <input class="form-control compte-form" type="text" placeholder="<?= $_SESSION['auth']->email; ?>" disabled>
                                </div>
                            </div>

                            <!-- Formulaire changement mot de passe -->
                            <h4 class="title-compte">Changement du mot de passe</h4>
                            <div class="row" style="margin-top:40px;">
                                <div class="form-group col-lg-4">
                                    <input class="form-control compte-form" type="password" name="password" placeholder="Nouveau mot de passe">
                                </div>

                                <div class="form-group col-lg-4">
                                    <input class="form-control compte-form" type="password" name="password_confirm" placeholder="Confirmation du mot de passe">
                                </div>

                                <div class="form-group col-lg-4">
                                   <button type="submit" class="btn btn-success btn-inscription">Valider</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

                <!-- Colonne -->
                <div class="col-md-4">
                    <div class="sup-side">
                        <h6 class="title-side">Informations</h6>
                        <a href="deconnexion.php" class="btn btn-success btn-deconnexion"><i class="fas fa-sign-out-alt" style="color:white;margin-right: 10px;"></i>Se déconnecter</a>
                    </div>
                    <div class="sup-side">
                        <h6 class="title-side">Discord</h6>
                        <a href="https://discord.gg/bUSvAev" target="_blank" class="btn btn-success btn-discord"><i class="fas fa-arrow-circle-right" style="color:white;margin-right: 10px;"></i>Rejoindre le discord</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<!-- Footer -->
<?php include('../inc/footer.php'); ?>
