<!-- Head/Navbar -->
<?php require_once('../inc/head.php'); ?>

<META HTTP-EQUIV="Refresh" CONTENT="5;URL=https://vultanium.fr">

<!-- Header -->
<header class="header">
	<div class="container">
		<div class="content-header" style="padding-top: 10%;padding-bottom: 12%;" data-aos="fade-down">
			<img src="../assets/img/logo/logo.png" class="img-responsive header-logo" alt="logo">

			<p style="text-align: center;color: white;">Vous êtes maitnenant inscrit à la bêta fermée de vultanium.</p>
			<p style="text-align: center;color: white;">Vous avez reçu un mail avec toutes les informations nécéssaires concernant la bêta fermée. Merci d'en prendre connaissance.</p>
			<p style="text-align: center;color: white;">A bientôt sur Vultanium !</p>

			<br>

			<hr>

			<p style="text-align: center;color: white;">Redirection automatique dans 5 secondes ...</p>

		</div>
	</div>
</header>


<!-- Footer -->
<?php include('../inc/footer.php'); ?>