<!-- Head/Navbar -->
<?php require_once('../inc/head.php'); ?>

<!-- Header -->
<header class="header">
	<div class="container">
		<div class="content-header" style="padding-top: 8%;padding-bottom: 12%;" data-aos="fade-down">
			<img src="../assets/img/logo/logo.png" class="img-responsive header-logo" alt="logo">

			<p style="color: white; text-align: center;text-transform: uppercase;letter-spacing: 1px;padding-bottom: 1%;">Fermeture des inscriptions le 02 janvier 2019</p>
			<p style="color: white; text-align: center;text-transform: uppercase;letter-spacing: 1px;"> [COMPLET]</p>

		<form action="/modules/form-beta" method="POST">

		 	<div class="form-row">

			<?php if(isset($_SESSION['auth'])): ?>
			    <div class="form-group col-md-12">
			      <label for="betamail" style="color:#fff;">Votre adresse mail <small style="color:red;">*</small></label>
			      <input type="text" class="form-control" id="betamail" name="betamail" value="<?= $_SESSION['auth']->email; ?>" required style="width: 50%;" disabled>
			    </div>
			<?php else: ?>
			    <div class="form-group col-md-12">
			      <label for="betamail" style="color:#fff;">Votre adresse mail <small style="color:red;">*</small></label>
			      <input type="text" class="form-control" id="betamail" name="betamail" required style="width: 50%;" disabled>
			    </div>
			<?php endif; ?>

			</div>


		  <hr>

		  <input type="submit" name="send-beta" id="send-beta" class="btn btn-recrutement" value="M'inscrire pour la bêta fermée" disabled>

		</form>


		<!-- <a href="#" class="connect-serveur" style="background: #ffa50054;color: white;padding: 24px;"><i class="fas fa-user-plus" style="color:#fff;"></i> M'inscrire à la Bêta fermée</a> -->

		</div>
	</div>
</header>




































<!-- Footer -->
<?php include('../inc/footer.php'); ?>